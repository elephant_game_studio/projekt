﻿using UnityEngine;
using System.Collections;

public class basicMonster : monster, IKillable
{


    public GameObject clone;
    public bool second = false;
    // Use this for initialization
    void Start()
    {
        this.health = 3;

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDemage(int demageTaken)
    {
        health -= demageTaken;
        if (health <= 0)
        {
            Kill();
        }
    }

    public override void Kill()
    {

        GameObject temp = Instantiate(particle, transform.position, transform.rotation) as GameObject;
        if (!second)
        {
            GameObject sec = Instantiate(clone, transform.position, transform.rotation) as GameObject;
            sec.GetComponent<basicMonster>().second = true;
        }
        Destroy(temp, 3f);
        Destroy(gameObject);
    }
}
