﻿using UnityEngine;
using System.Collections;

public class monster : MonoBehaviour {

    public float health;
    public GameObject particle;

    public virtual void Kill()
    {
        GameObject temp = Instantiate(particle, transform.position, transform.rotation) as GameObject;
        Destroy(temp, 3f);
        Destroy(gameObject);
    }
}
