﻿using UnityEngine;
using System.Collections;

public interface IKillable
{
    void TakeDemage(int damageTaken);
}
