﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class shootToPlayer : MonoBehaviour {


    public GameObject bullet;
    List<GameObject> bulletList = new List<GameObject>();

    bool canShoot = false;
    bool lockTarget = true;
    int monsterBullet = 2;

    Vector3 playerPos;
    // Use this for initialization
    void Start () {
        for (int i = 0; i < 3; i++)
        {
            bulletList.Add(Instantiate(bullet, transform.position, transform.rotation) as GameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            
            if (lockTarget)
            {
                
                playerPos = col.transform.position;
                lockTarget = false;
                StartCoroutine(WaitForShoot());
            }

            if (canShoot)
            {
                Shoot();
                canShoot = false;
                lockTarget = true;
            }
        }
    }

    void Shoot()
    {
        var myPos = transform.position;
        myPos.y += transform.parent.GetComponent<SphereCollider>().radius * transform.parent.transform.localScale.x + 1f;
        var heading = playerPos - myPos;
        var distance = heading.magnitude;
        var direction = heading / distance;

        foreach (GameObject bull in bulletList)
        {
            if (!bull.activeInHierarchy)
            {
                bull.transform.position = myPos;
                bull.SetActive(true);
                bullet bulletS = bull.GetComponent<bullet>();
                bulletS.type = monsterBullet;
                bulletS.AddForce(direction, 30f);
                break;
            }
        }
    }

    IEnumerator WaitForShoot()
    {
        yield return new WaitForSeconds(2f);
        canShoot = true;
    }
}
