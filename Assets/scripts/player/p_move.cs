﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class p_move : MonoBehaviour, IKillable
{


    float directionH;
    float directionV;
    float rotationSpeed = 80f;
    float moveSpeed = 7f;

    int health = 10;

    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Horizontal"))
        {
            directionH = Input.GetAxis("Horizontal");
            MoveHorizontal();
        }

        if (Input.GetButton("Vertical"))
        {
            directionV = Input.GetAxis("Vertical");
            MoveVertical();
        } 
    }

    void MoveHorizontal()
    {
        float shift = rotationSpeed * directionH * Time.deltaTime;
        transform.Rotate(0f, shift, 0f);
    }

    void MoveVertical()
    {
        transform.position += transform.forward * Time.deltaTime * moveSpeed * directionV;
    }

    

    public void TakeDemage(int demageTaken)
    {
        health -= demageTaken;
        if (health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
