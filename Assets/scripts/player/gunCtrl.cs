﻿using UnityEngine;
using System.Collections;
using System;

public class gunCtrl : MonoBehaviour {

    public GameObject gun;
    gun gunS;
    // Use this for initialization
    void Start () {
        gunS = gun.GetComponent<gun>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1"))
        {
            Shoot();
        }

        if (Input.GetButtonDown("First"))
        {
            gunS.AmmoType = 0;
        }

        if (Input.GetButtonDown("Second"))
        {
            gunS.AmmoType = 1;
        }
    }

    void Shoot()
    {
        gun.GetComponent<gun>().Shoot();
    }
}
