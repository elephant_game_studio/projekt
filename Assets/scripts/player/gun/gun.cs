﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class gun : MonoBehaviour {

    public int AmmoType { get; set; }

    public Transform muzzle;
    public GameObject bullet;

    List<GameObject> bulletList = new List<GameObject>();

    bool canShoot = true;
    float delay = 0.3f;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < 10; i++)
        {
            bulletList.Add(Instantiate(bullet, transform.position, transform.rotation) as GameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Shoot()
    {
        if (canShoot)
        {
            foreach (GameObject bull in bulletList)
            {
                if (!bull.activeInHierarchy)
                {
                    bull.transform.position = muzzle.transform.position;
                    bull.SetActive(true);
                    bullet bulletS = bull.GetComponent<bullet>();
                    bulletS.type = AmmoType;
                    bulletS.AddForce(transform.parent.transform.forward);
                    break;
                }
            }
            canShoot = false;
            StartCoroutine(ShootDelay());
        }       
    }

    IEnumerator ShootDelay()
    {
        yield return new WaitForSeconds(delay);
        canShoot = true;
    }
}
