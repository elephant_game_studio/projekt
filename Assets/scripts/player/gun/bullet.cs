﻿using UnityEngine;
using System.Collections;
using System;

public class bullet : MonoBehaviour {

    enum AmmoTypes {Normal, Power, Monster}

    public Material[] materials;
    Rigidbody rb;

    int demage = 2;

    public int type {
        set {
            Renderer rend = GetComponent<Renderer>();
            switch (value)
            {
                case (int)AmmoTypes.Normal:
                    demage = 2;
                    rend.sharedMaterial = materials[(int)AmmoTypes.Normal];
                    break;
                case (int)AmmoTypes.Power:
                    demage = 4;
                    rend.sharedMaterial = materials[(int)AmmoTypes.Power];
                    break;
                case (int)AmmoTypes.Monster:
                    demage = 1;
                    rend.sharedMaterial = materials[(int)AmmoTypes.Monster];
                    break;
            }
        }
    }


    public void AddForce(Vector3 dir, float force = 40f)
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(dir * force);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name != "rangeCollider")
        {
            try
            {
                IKillable temp = col.gameObject.transform.parent.GetComponent<IKillable>() as IKillable;
                temp.TakeDemage(demage);
            }

            catch (Exception e)
            {
                Debug.Log("nie do zabicia");
            }
        }

        if (col.gameObject.tag == "ground" ||
            col.gameObject.tag == "obsti" ||
            col.gameObject.tag == "monster" ||
            col.gameObject.tag == "Player")
        {
            rb.velocity = Vector3.zero;
            gameObject.SetActive(false);
        }
        
    }
}
